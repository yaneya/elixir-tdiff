defmodule Tdiff do

  def diff(sx, sy) do
    sxlen = length(sx)
    sylen = length(sy)
    dmax = sxlen + sylen
    case try_dpaths(0, dmax, [{0, 0, sx, sy, []}]) do
      :no -> [{:del, sx}, {:ins, sy}]
      {:ed, editopsr} -> edit_ops_to_edit_script(editopsr)
    end
  end

  defp try_dpaths(d, dmax, d1paths) when d <= dmax do
    case try_kdiagonals(-d, d, d1paths, []) do
      {:ed, e} -> {:ed, e}
      {:dpaths, dpaths} -> 
        try_dpaths(d+1, dmax, dpaths)
    end
  end
  defp try_dpaths(_, _dmax, _d1paths) do
    :no
  end

  defp try_kdiagonals(k, d, d1paths, dpaths) when k <= d do
    dpath = case d == 0 do 
      true -> hd(d1paths)
      false -> pick_best_dpath(k, d, d1paths)
    end
    case follow_snake(dpath) do
        {:ed, e} -> {:ed, e}
        {:dpath, dpath2} when k !== -d ->
            try_kdiagonals(k+2, d, tl(d1paths), [dpath2|dpaths])
        {:dpath, dpath2} when k == -d ->
            try_kdiagonals(k+2, d, d1paths, [dpath2|dpaths])
    end
  end
  defp try_kdiagonals(_, d, _, dpaths) do
    {:dpaths, :lists.reverse(dpaths)}
  end

  defp follow_snake({x, y, [h|tx], [h|ty], cs}) do 
    follow_snake({x+1, y+1, tx,ty,[{:e,h}|cs]}) 
  end

  defp follow_snake({_x, _y, [], [], cs}) do {:ed, cs} end
  defp follow_snake({x, y, [], sy, cs}) do {:dpath, {x, y, [], sy, cs}} end
  defp follow_snake({x, y, :oob, sy, cs}) do {:dpath, {x, y, :oob, sy, cs}} end
  defp follow_snake({x, y, sx, [], cs}) do {:dpath, {x, y, sx, [], cs}} end
  defp follow_snake({x, y, sx, :oob, cs}) do {:dpath, {x, y, sx, :oob, cs}} end
  defp follow_snake({x, y, sx, sy, cs}) do {:dpath, {x, y, sx, sy, cs}} end

  defp pick_best_dpath(k, d, dps) do pbd(k, d, dps) end

  defp pbd(k, d, [dp|_]) when k == -d do go_inc_y(dp) end
  defp pbd(k, d, [dp])   when k == d  do go_inc_x(dp) end
  defp pbd(_k, _d, [dp1, dp2|_]) do pbd2(dp1, dp2) end

  defp pbd2({_, y1, _, _, _} = dp1, {_, y2, _, _, _}) when y1 > y2 do go_inc_x(dp1) end
  defp pbd2(_dp1, dp2) do go_inc_y(dp2) end

  defp go_inc_y({x, y, [h|tx], sy, cs}) do {x, y+1, tx, sy, [{:y,h}|cs]} end
  defp go_inc_y({x, y, [], sy, cs}) do {x, y+1, :oob, sy, cs} end
  defp go_inc_y({x, y, :oob, sy, cs}) do {x, y+1, :oob, sy, cs} end

  defp go_inc_x({x, y, sx, [h|ty], cs}) do {x+1, y, sx, ty, [{:x,h}|cs]} end
  defp go_inc_x({x, y, sx, [], cs}) do {x+1, y, sx, :oob, cs} end
  defp go_inc_x({x, y, sx, :oob, cs}) do {x+1, y, sx,:oob, cs} end

  defp edit_ops_to_edit_script(editops) do e2e(editops, _acc=[]) end

  defp e2e([{:x, c}|t], [{:ins,r}|acc]) do e2e(t, [{:ins,[c|r]}|acc]) end
  defp e2e([{:y, c}|t], [{:del,r}|acc]) do e2e(t, [{:del,[c|r]}|acc]) end
  defp e2e([{:e, c}|t], [{:eq,r}|acc]) do e2e(t, [{:eq, [c|r]}|acc]) end
  defp e2e([{:x, c}|t], acc) do e2e(t, [{:ins,[c]}|acc]) end
  defp e2e([{:y, c}|t], acc) do e2e(t, [{:del,[c]}|acc]) end
  defp e2e([{:e, c}|t], acc) do e2e(t, [{:eq, [c]}|acc]) end
  defp e2e([], acc) do acc end

  def patch(s, diff) do p2(s, diff, []) end

  defp p2(s, [{:eq, t}|rest], acc) do p2_eq(s, t, rest, acc) end
  defp p2(s, [{:ins, t}|rest], acc) do p2_ins(s, t, rest, acc) end
  defp p2(s, [{:del, t}|rest], acc) do p2_del(s, t, rest, acc) end
  defp p2([], [], acc) do :lists.reverse(acc) end

  defp p2_eq([h|s], [h|t], rest, acc) do p2_eq(s, t, rest, [h|acc]) end
  defp p2_eq(s, [], rest, acc) do p2(s, rest, acc) end

  defp p2_ins(s, [h|t], rest, acc) do p2_ins(s, t, rest, [h|acc]) end
  defp p2_ins(s, [], rest, acc) do p2(s, rest, acc) end

  defp p2_del([h|s], [h|t], rest, acc) do p2_del(s, t, rest, acc) end
  defp p2_del(s, [], rest, acc) do p2(s, rest, acc) end

end
