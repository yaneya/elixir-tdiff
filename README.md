# Tdiff

Elixir fork of [tdiff](https://github.com/tomas-abrahamsson/tdiff) erlang library 

## Usage

```
iex -S mix
iex(1)> d = Tdiff.diff('cat', 'hat')
[del: 'c', ins: 'h', eq: 'at'] 
iex(2)> Tdiff.patch('cat', d) ## get origin text from diff
'hat'
```

## Installation

**Note: currently not available in Hex, add library like git dependency**

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `tdiff` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:tdiff, "~> 0.0.1"}]
    end
    ```

  2. Ensure `tdiff` is started before your application:

    ```elixir
    def application do
      [applications: [:tdiff]]
    end
    ```

