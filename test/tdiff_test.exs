defmodule TdiffTest do
  use ExUnit.Case
  doctest Tdiff

  test "basic diff" do
    r = [{:eq,'a'},{:del,'B'},{:ins,'X'},{:eq,'ccc'},{:del,'D'},{:ins,'Y'},{:eq,'e'}]
    assert r === Tdiff.diff('aBcccDe', 'aXcccYe')
  end
  
  test "completely mismatching" do
    assert [{:del,'aaa'}, {:ins,'bbb'}] === Tdiff.diff('aaa', 'bbb')
  end

  test "empty inputs produces empty diff" do
     assert [] === Tdiff.diff('', '')
  end
  
  test "only additions" do
    assert [{:ins,'aaa'}] === Tdiff.diff('', 'aaa')
    r = [{:eq,'a'},{:ins,'b'},{:eq,'a'},{:ins,'b'},{:eq,'a'},{:ins,'b'},{:eq,'a'}]
    assert r === Tdiff.diff('aaaa', 'abababa')
  end

  test "only deletions" do
    assert [{:del,'aaa'}] === Tdiff.diff('aaa', '')
    r = [{:eq,'a'},{:del,'b'},{:eq,'a'},{:del,'b'},{:eq,'a'},{:del,'b'},{:eq,'a'}]
    assert r === Tdiff.diff('abababa', 'aaaa')
  end

  test "patch" do
    s1 = [1, 2, 3]
    s2 = [3, 2, 1]
    r = Tdiff.diff(s1, s2)
    assert Tdiff.patch(s1, r) === s2
  end
end
